package edu.home.str;

public class BooleanequalsIgnoreCaseStringstring {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String S1 = new String("Health is importance of our life");
		String S2 = S1;
		String S3 = new String("Health is importance of our life");
		String S4 = new String("Health is importance of our life");
		boolean retVal;

		retVal = S1.equals(S2);
		System.out.println("Returned Value = " + retVal);

		retVal = S1.equals(S3);
		System.out.println("Returned Value = " + retVal);

		retVal = S1.equalsIgnoreCase(S4);
		System.out.println("Returned Value = " + retVal);
	}

}


